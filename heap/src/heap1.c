/*******************************************************************************
 * Цель – вызвать функцию foo
 ******************************************************************************/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <stdio.h>

void foo()
{
	printf("foo output\n");
}

int main(int argc, char *argv[])
{
	char *a,*b,*c;
	a = (char*)malloc(32);
	b = (char*)malloc(32);
	c = (char*)malloc(32);

	strcpy(a, argv[1]);
	strcpy(b, argv[2]);
	strcpy(c, argv[3]);

	free(c);
	free(b);
	free(a);

	printf("dynamite failed?\n");
}
